package controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.Application;
import view.GUI;

public class Controller {
	private GUI g;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addButton1Listener(new Action1());
		this.g.addButton2Listener(new Action2());
		this.g.addButton3Listener(new Action3());
		this.g.addButton4Listener(new Action4());
	}
	
	public class Action1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try{
			    PrintWriter writer = new PrintWriter("1.txt", "UTF-8");
			    Application app = new Application();
			    writer.println("The number of distinct days that appear in the monitoring data: " + app.distinctDays());
			    writer.close();
			    File myFile = new File("C:/Users/biris_000/workspace/StreamProcessing/1.txt");
		        Desktop.getDesktop().open(myFile);
			} catch (IOException p1) {
				p1.printStackTrace();
			} 
		}
	}
	
	public class Action2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try{
			    PrintWriter writer = new PrintWriter("2.txt", "UTF-8");
			    Application app = new Application();
			    Map<String, Long> map = app.numberOfDistinctAction();
			    writer.println("The number of occurrences of each distinct action type: ");
			    for (Entry<String, Long> entry: map.entrySet())
			    	writer.println(entry.getKey() + ": " + entry.getValue());
			    writer.close();
			    File myFile = new File("C:/Users/biris_000/workspace/StreamProcessing/2.txt");
		        Desktop.getDesktop().open(myFile);
			} catch (IOException p1) {
				p1.printStackTrace();
			} 
		}
	}
	
	public class Action3 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try{
			    PrintWriter writer = new PrintWriter("3.txt", "UTF-8");
			    Application app = new Application();
			    Map<Integer, Map<String, Long>> map = app.activitiesOnDays();
			    writer.println("Activity count for each day of the log: ");
				for (Entry<Integer, Map<String, Long>> entry : map.entrySet()) {
					 writer.println("Day " + entry.getKey());
				    for(Entry<String, Long> entry2 : entry.getValue().entrySet()) {
				    	 writer.println(entry2.getKey() + ": " + entry2.getValue());
				    }
				}
			    writer.close();
			    File myFile = new File("C:/Users/biris_000/workspace/StreamProcessing/3.txt");
		        Desktop.getDesktop().open(myFile);
			} catch (IOException p1) {
				p1.printStackTrace();
			} 
		}
	}
	
	public class Action4 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try{
			    PrintWriter writer = new PrintWriter("4.txt", "UTF-8");
			    Application app = new Application();
			    List<Entry<String, Long>> list =  app.totalDuration();
			    writer.println("Activities with total duration larger than 10 hours: ");
				for (Entry<String, Long> entry: list) {
					writer.println(entry.getKey() + ": " + entry.getValue() / (60 * 60 * 1000) + " hours");
				}
			    writer.close();
			    File myFile = new File("C:/Users/biris_000/workspace/StreamProcessing/4.txt");
		        Desktop.getDesktop().open(myFile);
			} catch (IOException p1) {
				p1.printStackTrace();
			} 
		}
	}
}
