package controller;

import view.GUI;

public class Main {
	
	public static void main(String[] args) {
		GUI g = new GUI();
		Controller contr = new Controller(g);
		g.setVisible(true);
	}

}
