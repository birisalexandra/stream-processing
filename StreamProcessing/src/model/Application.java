package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Application {
	private ArrayList<MonitoredData> list;
	
	public Application() {
		list = new ArrayList<MonitoredData>();
		readData();
	}
	
	public void readData() {
		try {
			BufferedReader in = new BufferedReader(new FileReader("Activities.txt"));
			String stringLine;
			while((stringLine = in.readLine()) != null) {
				StringTokenizer str = new StringTokenizer(stringLine);
				while(str.hasMoreTokens()) {
					SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				    Date dateAndHour1 = (Date) dt.parse(str.nextToken() + " " + str.nextToken());
				    Date dateAndHour2 = (Date) dt.parse(str.nextToken() + " " + str.nextToken());
					MonitoredData data = new MonitoredData(dateAndHour1, dateAndHour2, str.nextToken());
					list.add(data);
				}
			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public int distinctDays() {
		@SuppressWarnings("deprecation")
		int days = (int) list.stream().map(d -> d.getStartTime().getDate()).distinct().count();
		//System.out.println(days);
		return days;
	}
	
	public Map<String, Long> numberOfDistinctAction() {
		List<String> activities = list.stream().map(s -> s.getActivityLabel()).collect(Collectors.toList());
		Map<String, Long> map = activities.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		return map;
	}
	
	public Map<Integer, Map<String, Long>> activitiesOnDays() {
		@SuppressWarnings("deprecation")
		Map<Integer, Map<String, Long>> mapa = list.stream().collect(Collectors.groupingBy(d->d.getStartTime().getDate(), Collectors.groupingBy(s -> s.getActivityLabel(), Collectors.counting())));
		return mapa;
	}
	
	public long calculateDuration(Date start, Date end) {
		long diff = end.getTime() - start.getTime();
		return diff;
	}
	
	public List<Entry<String, Long>> totalDuration() {
		Map<String, Long> map = list.stream().collect(Collectors.groupingBy(s->s.getActivityLabel(), Collectors.summingLong(d->calculateDuration(d.getStartTime(), d.getEndTime()))));
		List<Entry<String, Long>> map2 = map.entrySet().stream().filter(d-> d.getValue() / (60 * 60 * 1000) > 10).collect(Collectors.toList());
		return map2;
	}
}
