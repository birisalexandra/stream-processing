package model;
import java.util.Date;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(Date t1, Date t2, String label) {
		this.startTime = t1;
		this.endTime = t2;
		this.activityLabel = label;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	
}
