package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class GUI extends JFrame {
	private JPanel contentPane = new JPanel();
	private JButton button = new JButton("1");
	private JButton button_1 = new JButton("2");
	private JButton button_2 = new JButton("3");
	private JButton button_3 = new JButton("4");
	private JButton button_4 = new JButton("5");
	
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		button.setBounds(177, 46, 89, 23);
		contentPane.add(button);
		
		button_1.setBounds(177, 80, 89, 23);
		contentPane.add(button_1);
		
		button_2.setBounds(177, 114, 89, 23);
		contentPane.add(button_2);
		
		button_3.setBounds(177, 148, 89, 23);
		contentPane.add(button_3);
		
		button_4.setBounds(177, 182, 89, 23);
		contentPane.add(button_4);
		
		JLabel lblDistinctDays = new JLabel("Distinct days");
		lblDistinctDays.setBounds(51, 50, 92, 14);
		contentPane.add(lblDistinctDays);
		
		JLabel lblNumberOfOccurences = new JLabel("Number of occurrences");
		lblNumberOfOccurences.setBounds(22, 84, 145, 14);
		contentPane.add(lblNumberOfOccurences);
		
		JLabel lblActivityForEach = new JLabel("Activity for each day");
		lblActivityForEach.setBounds(32, 118, 126, 14);
		contentPane.add(lblActivityForEach);
		
		JLabel lblDuration = new JLabel("Duration");
		lblDuration.setBounds(61, 152, 60, 14);
		contentPane.add(lblDuration);
		
		JLabel lblFilterActivities = new JLabel("Filter activities");
		lblFilterActivities.setBounds(53, 186, 90, 14);
		contentPane.add(lblFilterActivities);
	}
	
	public void addButton1Listener(ActionListener a) {
		button.addActionListener(a);
	}
	public void addButton2Listener(ActionListener a) {
		button_1.addActionListener(a);
	}
	public void addButton3Listener(ActionListener a) {
		button_2.addActionListener(a);
	}
	public void addButton4Listener(ActionListener a) {
		button_3.addActionListener(a);
	}
	public void addButton5Listener(ActionListener a) {
		button_4.addActionListener(a);
	}
}
